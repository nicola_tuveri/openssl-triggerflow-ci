# RSA

## RSA: generate private key
debug openssl genpkey -algorithm RSA -out private_key.pem -pkeyopt rsa_keygen_bits:2048

## RSA: generate public key
debug openssl rsa -in private_key.pem -pubout -out public_key.pem

## RSA: sign
debug openssl dgst -sha512 -sign private_key.pem -out lsb-release.sig data

## RSA: encrypt (not secret)
openssl rsautl -encrypt -pubin -inkey public_key.pem -in data -out lsb-release.bin

## RSA: decrypt
debug openssl rsautl -decrypt -inkey private_key.pem -in lsb-release.bin -out lsb-release

## RSA MP: generate private key
debug openssl genrsa -out rsamptest.pem 2048

## RSA: check key params
debug openssl rsa -in rsamptest.pem -check
debug openssl pkey -in rsamptest.pem -check

# ECDSA

## ECDSA: generate private key P-192
debug openssl genpkey -algorithm EC -out prime192v1.pem -pkeyopt ec_paramgen_curve:prime192v1

## ECDSA: sign P-192
debug openssl dgst -sha512 -sign prime192v1.pem -out lsb-release.sig data

## ECDSA: generate private key P-224
debug openssl genpkey -algorithm EC -out secp224r1.pem -pkeyopt ec_paramgen_curve:secp224r1

## ECDSA: sign P-224
debug openssl dgst -sha512 -sign secp224r1.pem -out lsb-release.sig data

## ECDSA: generate private key P-256
debug openssl genpkey -algorithm EC -out prime256v1.pem -pkeyopt ec_paramgen_curve:prime256v1

## ECDSA: sign P-256
debug openssl dgst -sha512 -sign prime256v1.pem -out lsb-release.sig data

## ECDSA: generate private key P-384
debug openssl genpkey -algorithm EC -out secp384r1.pem -pkeyopt ec_paramgen_curve:secp384r1

## ECDSA: sign P-384
debug openssl dgst -sha512 -sign secp384r1.pem -out lsb-release.sig data

## ECDSA: generate private key P-521
debug openssl genpkey -algorithm EC -out secp521r1.pem -pkeyopt ec_paramgen_curve:secp521r1

## ECDSA: sign P-521
debug openssl dgst -sha512 -sign secp521r1.pem -out lsb-release.sig data

## ECDSA: generate private key secp256k1
debug openssl genpkey -algorithm EC -out secp256k1.pem -pkeyopt ec_paramgen_curve:secp256k1

## ECDSA: sign secp256k1
debug openssl dgst -sha512 -sign secp256k1.pem -out lsb-release.sig data

## ECDSA: generate private key B-233
debug openssl genpkey -algorithm EC -out sect233r1.pem -pkeyopt ec_paramgen_curve:sect233r1

## ECDSA: sign B-233
debug openssl dgst -sha512 -sign sect233r1.pem -out lsb-release.sig data

# DSA

## DSA: generate parameters (not secret)
exec openssl genpkey -genparam -algorithm DSA -out dsa.params -pkeyopt dsa_paramgen_bits:2048

## DSA: generate private key
debug openssl genpkey -paramfile dsa.params -out dsa.pkey

exec cat dsa.params dsa.pkey > dsa.pem

## DSA: sign
debug openssl dgst -sha512 -sign dsa.pem -out lsb-release.sig data

exec openssl ecparam -genkey -name prime256v1 -out ec.params
debug openssl ec -in ec.params -pubout -out ec.pkey
exec cat ec.params ec.pkey > ec.key
debug openssl req -x509 -new -key ec.key -subj '/C=FI/ST=Uusimaa/L=Helsinki/CN=localhost' -config openssl.cnf -out cert.pem
debug openssl cms -aes128 -encrypt -in data -binary -out lsb-release.pem -outform PEM -recip cert.pem -keyopt ecdh_kdf_md:sha256
debug openssl cms -decrypt -inkey ec.key -in lsb-release.pem -inform PEM -out lsb-release -recip cert.pem

# EC custom curves
debug openssl genpkey -algorithm EC -pkeyopt ec_paramgen_curve:P-256 -pkeyopt ec_param_enc:explicit -outform DER -out p256.der
sed -i 's/\xf3\xb9\xca\xc2\xfc\x63\x25\x51\x02\x01\x01/\xf3\xb9\xca\xc2\xfc\x63\x25\x51\x02\x01\x00/' p256.der
debug openssl dgst -sha256 -sign p256.der -keyform DER -out /dev/null data

debug openssl genpkey -algorithm EC -pkeyopt ec_paramgen_curve:P-384 -pkeyopt ec_param_enc:explicit -outform DER -out p384.der
sed -i 's/\xec\xec\x19\x6a\xcc\xc5\x29\x73\x02\x01\x01/\xec\xec\x19\x6a\xcc\xc5\x29\x73\x02\x01\x00/' p384.der
debug openssl dgst -sha512 -sign p384.der -keyform DER -out /dev/null data

debug openssl genpkey -algorithm EC -pkeyopt ec_paramgen_curve:secp256k1 -pkeyopt ec_param_enc:explicit -outform DER -out secp256k1.der
sed -i 's/\xbf\xd2\x5e\x8c\xd0\x36\x41\x41\x02\x01\x01/\xbf\xd2\x5e\x8c\xd0\x36\x41\x41\x02\x01\x00/' secp256k1.der
debug openssl dgst -sha256 -sign secp256k1.der -keyform DER -out /dev/null data

debug openssl genpkey -algorithm EC -pkeyopt ec_paramgen_curve:K-233 -pkeyopt ec_param_enc:explicit -outform DER -out k233.der
sed -i 's/\x6e\xfb\x1a\xd5\xf1\x73\xab\xdf\x02\x01\x04/\x6e\xfb\x1a\xd5\xf1\x73\xab\xdf\x02\x01\x00/' k233.der
debug openssl dgst -sha256 -sign k233.der -keyform DER -out /dev/null data

debug openssl genpkey -algorithm EC -pkeyopt ec_paramgen_curve:B-233 -pkeyopt ec_param_enc:explicit -outform DER -out b233.der
sed -i 's/\x22\x03\x1d\x26\x03\xcf\xe0\xd7\x02\x01\x02/\x22\x03\x1d\x26\x03\xcf\xe0\xd7\x02\x01\x00/' b233.der
debug openssl dgst -sha256 -sign b233.der -keyform DER -out /dev/null data


# PVK

## PVK: generate parameters (not secret)
exec openssl genpkey -genparam -algorithm DSA -out dsa.params -pkeyopt dsa_paramgen_bits:1024 -pkeyopt dsa_paramgen_q_bits:160

## PVK: generate DSA private key in PEM format
debug openssl genpkey -paramfile dsa.params -out dsa.pkey

## PVK: convert from PEM to PVK
debug openssl dsa -in dsa.pkey -outform PVK -pvk-none -out dsa.pvk

## PVK: sign
debug openssl dgst -sha1 -sign dsa.pvk -keyform PVK -out lsb-release.sig data

# MSBLOB

## MSBLOB: generate parameters (not secret)
exec openssl genpkey -genparam -algorithm DSA -out dsa.params -pkeyopt dsa_paramgen_bits:1024 -pkeyopt dsa_paramgen_q_bits:160

## MSBLOB: generate DSA private key in PEM format
debug openssl genpkey -paramfile dsa.params -out dsa.pkey

## MSBLOB: convert from PEM to PVK
debug openssl dsa -in dsa.pkey -outform MS\ PRIVATEKEYBLOB -out dsa.blob

## MSBLOB: sign
debug openssl dgst -sha1 -sign dsa.blob -keyform MS\ PRIVATEKEYBLOB -out lsb-release.sig data

# DH TODO

